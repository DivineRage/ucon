﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UConsole;

namespace Complete {
    public class ShotCounter : MonoBehaviour {

        [SerializeField]
        private int _playerIndex;
        private int _count;
        private Text _textField;

        [ConsoleCommand("shotcount", "", typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public int Count {
            get { return _count; }
            set {
                _count = value;
                _textField.text = _count.ToString();
            }
        }



        void Awake() {
            TankShooting.OnShotProjectile += TankShooting_OnShotProjectile;
            GameManager.OnResetShotCount += GameManager_OnReset;
            _textField = GetComponent<Text>();
        }

        void Start() {
            gameObject.name = "Player " + _playerIndex;
        }

        private void GameManager_OnReset() {
            Count = 0;
        }

        private void TankShooting_OnShotProjectile(int playerNumber) {
            if (playerNumber != _playerIndex)
                return;

            Count++;
        }
    }
}