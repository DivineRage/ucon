﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.CommandEntries {
    public class CommandEntryMethod : CommandEntryBase {

        public readonly MethodInfo methodInfo;

        public override Parameter[] ExpectedParameters {
            get {
                return methodInfo.GetParameters().Select(pi => new Parameter(pi, pi.IsOptional)).ToArray();
            }
        }
        public override Type DeclaringType {
            get {
                return methodInfo.DeclaringType;
            }
        }
        public override bool IsStatic { get { return methodInfo.IsStatic; } }
        public override AccessModifierFlags AccessModifiers {
            get {
                if (methodInfo.IsPublic)
                    return AccessModifierFlags.Public;
                if (methodInfo.IsAssembly)
                    return AccessModifierFlags.Internal;
                if (methodInfo.IsFamily)
                    return AccessModifierFlags.Protected;
                if (methodInfo.IsFamilyOrAssembly)
                    return AccessModifierFlags.Protected & AccessModifierFlags.Internal;
                if (methodInfo.IsPrivate)
                    return AccessModifierFlags.Private;

                throw new Exception("Unknown Access Modifiers");
            }
        }
        public override bool AllowInvokeWithoutArguments {
            get {
                var parameters = methodInfo.GetParameters();
                return parameters.Length == 0 || parameters.All(p => p.IsOptional);
            }
        }
        public override CommandEntryType CommandType { get { return CommandEntryType.Method; } }



        public CommandEntryMethod(ConsoleCommandAttribute attribute, MethodInfo methodInfo) :
            base(attribute.CommandName, attribute.CommandDescription, attribute, attribute.InstanceRetrievalMethod) {
            this.methodInfo = methodInfo;
        }

        protected override UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue) {
            if (arguments == null)
                arguments = new object[0];

            var parameters = methodInfo.GetParameters();

            // If this method doesn't take any parameters.
            if (parameters.Length == 0) {
                returnValue = methodInfo.Invoke(target, null);
                return UCon.CommandExecutionResult.Success;
            }

            // If no arguments are supplied and all parameters are optional.
            if (arguments.Length == 0 && parameters.All(p => p.IsOptional)) {
                returnValue = methodInfo.Invoke(target, parameters.Select(p => p.DefaultValue).ToArray());
                return UCon.CommandExecutionResult.Success;
            }

            var passedArguments = new object[parameters.Length];
            for (var i = 0; i < passedArguments.Length; i++) {
                if (i < arguments.Length) {
                    passedArguments[i] = arguments[i];
                }
                else if (parameters[i].IsOptional) {
                    passedArguments[i] = parameters[i].DefaultValue;
                }
                else {
                    returnValue = null;
                    return UCon.CommandExecutionResult.Unknown;
                }
            }

            returnValue = methodInfo.Invoke(target, passedArguments);
            return UCon.CommandExecutionResult.Success;
        }

    }
}