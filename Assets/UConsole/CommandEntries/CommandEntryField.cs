﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.CommandEntries {
    public class CommandEntryField : CommandEntryBase {

        public readonly FieldInfo fieldInfo;

        public override Parameter[] ExpectedParameters {
            get {
                return new Parameter[] { new Parameter(fieldInfo.FieldType, false) };
            }
        }
        public override Type DeclaringType {
            get {
                return fieldInfo.DeclaringType;
            }
        }
        public override bool IsStatic {
            get {
                return fieldInfo.IsStatic;
            }
        }
        public override AccessModifierFlags AccessModifiers {
            get {
                if (fieldInfo.IsPublic)
                    return AccessModifierFlags.Public;
                if (fieldInfo.IsAssembly)
                    return AccessModifierFlags.Internal;
                if (fieldInfo.IsFamily)
                    return AccessModifierFlags.Protected;
                if (fieldInfo.IsFamilyOrAssembly)
                    return AccessModifierFlags.Protected & AccessModifierFlags.Internal;
                if (fieldInfo.IsPrivate)
                    return AccessModifierFlags.Private;

                throw new Exception("Unknown Access Modifiers");
            }
        }
        public override bool AllowInvokeWithoutArguments {
            get {
                return true;
            }
        }
        public override CommandEntryType CommandType { get { return CommandEntryType.Field; } }


        public CommandEntryField(ConsoleCommandAttribute attribute, FieldInfo fieldInfo) :
            base(attribute.CommandName, attribute.CommandDescription, attribute, attribute.InstanceRetrievalMethod) {
            this.fieldInfo = fieldInfo;
        }

        protected override UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue) {
            if (arguments == null || arguments.Length == 0) {
                // Print value
                returnValue = fieldInfo.GetValue(target);
                Debug.LogFormat("{0}: {1}", CommandName, returnValue);
                return UCon.CommandExecutionResult.Success;
            }
            else {
                // Set value
                try {
                    fieldInfo.SetValue(target, arguments[0]);
                    returnValue = fieldInfo.GetValue(target);
                    Debug.LogFormat("{0}: {1}", CommandName, returnValue);
                }
                catch (Exception e) {
                    UCon.LogException(e);
                    returnValue = null;
                    return UCon.CommandExecutionResult.ThrewException;
                }
                return UCon.CommandExecutionResult.Success;
            }
        }

    }
}