﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.CommandEntries {
    public class CommandEntryAlias : CommandEntryBase {

        public readonly string Command;

        public override AccessModifierFlags AccessModifiers {
            get {
                return AccessModifierFlags.Public;
            }
        }

        public override bool AllowInvokeWithoutArguments {
            get {
                return true;
            }
        }

        public override Parameter[] ExpectedParameters {
            get {
                return new Parameter[0];
            }
        }

        public override Type DeclaringType {
            get {
                throw new Exception("Declaring type should not be required for Alias commands.");
            }
        }

        public override bool IsStatic {
            get {
                return true;
            }
        }

        public override CommandEntryType CommandType {
            get {
                return CommandEntryType.Alias;
            }
        }

        public virtual UCon.CommandExecutionResult Invoke(UCon.ExecutionCommand exCommand, out object returnValue) { 
            if (exCommand == null || exCommand.ArgumentCount == 0) {
                returnValue = UCon.Execute(Command);
                return UCon.CommandExecutionResult.Success;
            } else {
                var c = Command;
                while(exCommand.ArgumentsLeftToDequeue > 0) {
                    var arg = exCommand.Dequeue();
                    c += string.Format(" \"{0}\"", arg.AsString);
                }
                returnValue = UCon.Execute(c);
                return UCon.CommandExecutionResult.Success;
            }
        }

        protected override UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue) {
            throw new NotImplementedException();
        }

        public CommandEntryAlias(string alias, string command) :
            base(alias, command, null, null) {
            Command = command;
        }

    }
}