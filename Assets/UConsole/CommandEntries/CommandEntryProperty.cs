﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UConsole.CommandEntries {
    public class CommandEntryProperty : CommandEntryBase {

        public readonly PropertyInfo propertyInfo;

        public override Parameter[] ExpectedParameters {
            get {
                return (propertyInfo.CanWrite ? propertyInfo.GetSetMethod(true).GetParameters() : new ParameterInfo[0])
                    .Select(pi => new Parameter(pi, pi.IsOptional)).ToArray();
            }
        }
        public override Type DeclaringType {
            get {
                return propertyInfo.DeclaringType;
            }
        }
        public override bool IsStatic {
            get {
                return propertyInfo.CanRead ? propertyInfo.GetGetMethod(true).IsStatic : propertyInfo.GetSetMethod(true).IsStatic;
            }
        }
        public override AccessModifierFlags AccessModifiers {
            get {
                var method = (propertyInfo.CanRead ? propertyInfo.GetGetMethod(true) : propertyInfo.GetSetMethod(true));
                if (method.IsPublic)
                    return AccessModifierFlags.Public;
                if (method.IsAssembly)
                    return AccessModifierFlags.Internal;
                if (method.IsFamily)
                    return AccessModifierFlags.Protected;
                if (method.IsFamilyOrAssembly)
                    return AccessModifierFlags.Protected & AccessModifierFlags.Internal;
                if (method.IsPrivate)
                    return AccessModifierFlags.Private;

                throw new Exception("Unknown Access Modifiers");
            }
        }
        public override bool AllowInvokeWithoutArguments {
            get {
                return propertyInfo.CanRead;
            }
        }
        public override CommandEntryType CommandType { get { return CommandEntryType.Property; } }


        public CommandEntryProperty(ConsoleCommandAttribute attribute, PropertyInfo propertyInfo) :
            base(attribute.CommandName, attribute.CommandDescription, attribute, attribute.InstanceRetrievalMethod) {
            this.propertyInfo = propertyInfo;
        }

        protected override UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue) {
            if (arguments == null)
                arguments = new object[0];

            returnValue = null;

            if (arguments.Length == 0) {
                if (AllowInvokeWithoutArguments) {
                    // Print value
                    returnValue = propertyInfo.GetGetMethod(true).Invoke(target, null);
                    Debug.LogFormat("{0} == {1}", CommandName, returnValue);
                    return UCon.CommandExecutionResult.Success;
                }
                else {
                    // Tried to read when no getter.
                    UCon.LogWarningFormat("Tried to read a Property when that Property has no getter: {0}", CommandName);
                    return UCon.CommandExecutionResult.Unknown;
                }
            }
            else if (arguments.Length == 1) {
                // Set value
                try {
                    propertyInfo.GetSetMethod(true).Invoke(target, arguments);
                    Debug.LogFormat("{0} = {1}", CommandName, arguments[0]);
                }
                catch (Exception e) {
                    UCon.LogException(e);
                    return UCon.CommandExecutionResult.ThrewException;
                }
                return UCon.CommandExecutionResult.Success;
            }
            else {
                // Too many supplied arguments.
                UCon.LogWarningFormat("Invoked Property command with more than one ({1}) argument: {0}", CommandName, arguments.Length);
                return UCon.CommandExecutionResult.Unknown;
            }
        }
    }
}