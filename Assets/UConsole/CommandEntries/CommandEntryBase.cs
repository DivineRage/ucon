﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UConsole.Config;
using UConsole.InstanceRetrieval;

namespace UConsole.CommandEntries {
    public abstract class CommandEntryBase {

        public readonly string CommandName;
        public readonly string CommandDescription;
        public readonly ConsoleCommandAttribute CommandAttribute;
        public readonly Type InstanceRetrievalMethodType;

        public abstract Parameter[] ExpectedParameters { get; }
        public abstract Type DeclaringType { get; }
        public abstract bool IsStatic { get; }
        public abstract AccessModifierFlags AccessModifiers { get; }
        public abstract bool AllowInvokeWithoutArguments { get; }
        public virtual bool IsExtensionMethod { get { return false; } }
        public virtual CommandEntryType CommandType { get { return CommandEntryType.Unknown; } }
        public int RequiredArgumentCount {
            get {
                // Retrieval Method MUST exist. We verified this when adding the command!
                return (InstanceRetrievalMethodType == null ? 0 : UCon.GetInstanceRetrievalMethod(InstanceRetrievalMethodType).RequiredArgumentCount) +
                    ExpectedParameters.Count(p => !p.IsOptional);
            }
        }

        /// <summary>
        /// Is anyone safely allowed to look at the current value of this command. 
        /// False if it may result in odd behaviour.
        /// </summary>
        public virtual bool AllowedToLookAtValue { get { return false; } }

        protected CommandEntryBase(string name, string description, ConsoleCommandAttribute attribute, Type requiredType) {
            CommandName = name;
            CommandDescription = description;
            CommandAttribute = attribute;
            InstanceRetrievalMethodType = requiredType;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="arguments">Tokenized list of command arguments.</param>
        /// <returns>Invocation result.
        ///     UCon.CommandExecutionResult.Unknown if UConConfig.AllowMulticastCommands is enabled and multiple targets where found.
        ///     UCon.CommandExecutionResult.Success if a single invocation ended succesfully.
        ///     UCon.CommandExecutionResult.ThrewException if a single invocation ended with an exception being thrown.
        ///     UCon.CommandExecutionResult.NoTarget if no targets could be found.
        ///     UCon.CommandExecutionResult.TooManyTargets if more than one target was found and UConConfig.AllowMulticastCommands was false.
        ///     UCon.CommandExecutionResult.FailedToParseArguments if one or more arguments could not be parsed to their respective types.
        /// </returns>
        public virtual UCon.CommandExecutionResult Invoke(UCon.ExecutionCommand exCommand, out object returnValue) {
            object[] targets = null;

            if (exCommand.ArgumentCount < RequiredArgumentCount) {
                UCon.LogErrorFormat("({0}) Too few arguments provided to find target and invoke. ({1} < {2})",
                    exCommand.CommandName,
                    exCommand.ArgumentCount,
                    RequiredArgumentCount);
                returnValue = null;
                return UCon.CommandExecutionResult.TooFewArguments;
            }

            // Instance members
            if (IsStatic && !IsExtensionMethod) {
                targets = new object[] { null };
            }
            else {
                targets = GetInvokeTargets(exCommand);

                if (targets == null || targets.Length == 0) {
                    UCon.LogErrorFormat("({0}) No targets found.", exCommand.CommandName);
                    returnValue = null;
                    return UCon.CommandExecutionResult.NoTarget;
                }
            }

            object[] convertedArguments = null;
            if (GetConvertedArguments(exCommand, out convertedArguments) == ArgumentConversionResult.Failure) {
                UCon.LogErrorFormat("({0}) Failed to parse arguments.", exCommand.CommandName);
                returnValue = null;
                return UCon.CommandExecutionResult.FailedToParseArguments;
            }

            if (targets.Length == 1) {
                return InvokeOnTarget(targets[0], convertedArguments, out returnValue);
            }
            else {
                returnValue = null;

                // TODO: Ask for permission to multicast?
                for (int i = 0; i < targets.Length; i++) {
                    InvokeOnTarget(targets[i], convertedArguments, out returnValue);
                }

                return UCon.CommandExecutionResult.Success;
            }
        }

        protected abstract UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue);

        protected virtual object[] GetInvokeTargets(UCon.ExecutionCommand exCommand) {
            var retrievalMethodInstance = UCon.GetInstanceRetrievalMethod(InstanceRetrievalMethodType);

            if (exCommand.ArgumentsLeftToDequeue < retrievalMethodInstance.RequiredArgumentCount) {
                // Not enough arguments.
            }

            /* // Handle someone using a subcommand to get the required type.
            if (retrievalMethodInstance.RequiredArgumentCount > 0) {
                var arg = exCommand.Dequeue();
            }
            */

            var arguments = new string[retrievalMethodInstance.RequiredArgumentCount];
            for (int i = 0; i < arguments.Length; i++) {
                arguments[i] = exCommand.Dequeue().AsString;
            }

            return retrievalMethodInstance.GetInvokeTarget(DeclaringType, arguments);
        }

        protected virtual ArgumentConversionResult GetConvertedArguments(UCon.ExecutionCommand exCommand, out object[] convertedArguments) {
            if (exCommand.ArgumentCount == 0 && AllowInvokeWithoutArguments) {
                convertedArguments = new object[0];
                return ArgumentConversionResult.Success;
            }

            var expectedTypes = ExpectedParameters;

            // Make sure we have enough argument inputs to satisfy all parameters.
            for (int i = 0; i < expectedTypes.Length; i++) {
                // If we have no argument but require one, we can't proceed.
                if (i >= exCommand.ArgumentCount && !expectedTypes[i].IsOptional) {
                    convertedArguments = null;
                    return ArgumentConversionResult.Failure;
                }
            }

            // Should have enough arguments for all mandatory parameters. Start converting them.
            convertedArguments = new object[expectedTypes.Length];
            for (var i = 0; i < expectedTypes.Length; i++) {
                try {
                    var expectedType = expectedTypes[i];

                    // Should not have to check if the expected Type is optional, we did that before the loop.
                    if (exCommand.ArgumentsLeftToDequeue == 0) {
                        convertedArguments[i] = expectedType.DefaultValue;
                        continue;
                    }

                    var argument = exCommand.Dequeue();

                    /*
                    // If this is the last parameter
                    if (i == expectedTypes.Length - 1) {
                        // And we're one of these string enumerables, consider padding them with remaining 
                        if (expectedType.Type == typeof(string[])) {

                        }
                        else if (expectedType.Type == typeof(List<string>)) {

                        }
                    }
                    */

                    /*
                    // If the type is an array without a special converter, try and serialize as individual elements.
                    if (expectedType.Type.IsArray && !UCon.HasArgumentConverter(expectedType.Type)) {
                        // Handle Arrays by re-tokenizing and converting each individually.
                        if (!expectedType.Type.HasElementType)
                            throw new Exception("How can a type be an array but not have an element type?");

                        // Get the underlying type for this array.
                        var elementType = expectedType.Type.GetElementType();
                        var converter = UCon.GetArgumentConverter(elementType); // YOLO: Null refs are caught by try block.

                        // Re-tokenize the single string argument so it becomes individual elements.
                        // TODO: Do I keep this? Indicate it? Make it an option?
                        var tokenizedArgument = UCon.TokenizeString(arguments.Dequeue());

                        // Create the object[] to pass into the Activator.CreateInstance call, where we convert it to the desired type.
                        var elements = new object[tokenizedArgument.Count];
                        for (var elementIndex = 0; elementIndex < elements.Length; elementIndex++)
                            elements[elementIndex] = converter.Convert(tokenizedArgument);
                        var arrayInstance = Activator.CreateInstance(expectedType.Type, elements);
                        convertedArguments[i] = arrayInstance;
                    }
                    */

                    var argObj = argument.AsObject;
                    convertedArguments[i] = argObj;

                    if (argObj == null) {
                        continue;
                    }
                    else {
                        var argObjType = argObj.GetType();

                        if (argObjType == expectedType.Type) {
                            // Provided as the correct type
                            continue;
                        }

                        // Check for implicit conversion. So we can supply, for example, a Color32 as a Color.
                        // https://stackoverflow.com/questions/11544056/how-to-cast-implicitly-on-a-reflected-method-call
                        var implicitConverter = expectedType.Type.GetMethod("op_Implicit", new[] { argObjType });
                        if (implicitConverter != null) {
                            convertedArguments[i] = implicitConverter.Invoke(null, new object[] { argument });
                        }
                    }

                    if (argObj.GetType() != typeof(string)) {
                        UCon.LogWarningFormat("Non-string argument of type {0} did not match expected type {1}. Converting to string and attempting to convert...", 
                            argObj.GetType(), 
                            expectedType.Type);
                        argObj = argObj.ToString();
                    }

                    var argConverter = UCon.GetArgumentConverter(expectedType.Type);
                    convertedArguments[i] = argConverter.Convert((string) argObj);
                }
                catch (Exception e) {
                    UCon.LogException(e);
                    convertedArguments = null;
                    return ArgumentConversionResult.Failure;
                }
            }
            return ArgumentConversionResult.Success;
        }



        public class Parameter {

            public readonly Type Type;
            public readonly string ParameterName;
            public readonly bool IsOptional;
            public readonly bool IsParams;
            public readonly object DefaultValue;

            public Parameter(ParameterInfo paramInfo, bool isOptional) {
                Type = paramInfo.ParameterType;
                ParameterName = paramInfo.Name;
                IsOptional = isOptional;
                IsParams = paramInfo.GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0;
                DefaultValue = paramInfo.IsOptional ? paramInfo.DefaultValue : null;
            }
            public Parameter(Type type, bool isOptional) {
                Type = type;
                ParameterName = type.Name;
                IsOptional = isOptional;
                IsParams = false;
                DefaultValue = null;
            }
        }

        protected enum ArgumentConversionResult {
            Success,
            Failure,
        }

        public enum CommandEntryType {
            Unknown,
            Field,
            Property,
            Method,
            Event,
            Alias,
        }

        [Flags]
        public enum AccessModifierFlags {
            Public = 1,
            Protected = 2,
            Internal = 4,
            Private = 8,
        }

    }
}