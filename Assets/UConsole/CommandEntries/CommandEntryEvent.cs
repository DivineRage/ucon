﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.CommandEntries {
    public class CommandEntryEvent : CommandEntryBase {

        public readonly EventInfo eventInfo;

        public override Parameter[] ExpectedParameters {
            get {
                return eventInfo.EventHandlerType.GetMethod("Invoke").GetParameters().Select(pi => new Parameter(pi, pi.IsOptional)).ToArray();
            }
        }
        public override Type DeclaringType {
            get {
                return eventInfo.DeclaringType;
            }
        }
        public override bool IsStatic {
            get {
                return eventInfo.GetAddMethod().IsStatic;
            }
        }
        public override AccessModifierFlags AccessModifiers {
            get {
                if (eventInfo.GetAddMethod().IsPublic)
                    return AccessModifierFlags.Public;
                if (eventInfo.GetAddMethod().IsAssembly)
                    return AccessModifierFlags.Internal;
                if (eventInfo.GetAddMethod().IsFamily)
                    return AccessModifierFlags.Protected;
                if (eventInfo.GetAddMethod().IsFamilyOrAssembly)
                    return AccessModifierFlags.Protected & AccessModifierFlags.Internal;
                if (eventInfo.GetAddMethod().IsPrivate)
                    return AccessModifierFlags.Private;

                throw new Exception("Unknown Access Modifiers");
            }
        }
        public override bool AllowInvokeWithoutArguments {
            get {
                return ExpectedParameters.Length == 0;
            }
        }
        public override CommandEntryType CommandType { get { return CommandEntryType.Event; } }


        public CommandEntryEvent(ConsoleCommandAttribute attribute, EventInfo eventInfo) :
            base(attribute.CommandName, attribute.CommandDescription, attribute, attribute.InstanceRetrievalMethod) {
            this.eventInfo = eventInfo;
        }

        protected override UCon.CommandExecutionResult InvokeOnTarget(object target, object[] arguments, out object returnValue) {
            if (arguments == null)
                arguments = new object[0];

            var backingField = eventInfo.DeclaringType.GetField(eventInfo.Name, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.GetField);
            var fieldAsDelegate = (Delegate)backingField.GetValue(target);
            var invocationList = fieldAsDelegate.GetInvocationList();

            for (int iInvoker = 0; iInvoker < invocationList.Length; iInvoker++) {
                var methodInfo = invocationList[iInvoker].Method;
                target = invocationList[iInvoker].Target;
                var parameters = methodInfo.GetParameters();

                // If this method doesn't take any parameters.
                if (parameters.Length == 0) {
                    methodInfo.Invoke(target, null);
                }

                var passedArguments = new object[parameters.Length];
                for (var i = 0; i < passedArguments.Length; i++) {
                    if (i < arguments.Length) {
                        passedArguments[i] = arguments[i];
                    }
                    else if (parameters[i].IsOptional) {
                        passedArguments[i] = parameters[i].DefaultValue;
                    }
                    else {
                        returnValue = null;
                        return UCon.CommandExecutionResult.Unknown;
                    }
                }

                methodInfo.Invoke(target, passedArguments);
            }

            returnValue = null;
            return UCon.CommandExecutionResult.Success;
        }

    }
}