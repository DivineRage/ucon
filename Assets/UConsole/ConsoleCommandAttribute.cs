using System;
using System.Linq;
using UConsole.InstanceRetrieval;

namespace UConsole {
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event, AllowMultiple = true)]
    public sealed class ConsoleCommandAttribute : Attribute {

        public readonly string CommandName;
        public readonly string CommandDescription;

        public readonly Type InstanceRetrievalMethod;

        public ConsoleCommandAttribute(string name, string description = "", Type instanceRetrievalMethod = null) {
			if (name == null)
                throw new ArgumentNullException("name");

            name = name.Trim();

			if (name == "")
                throw new ArgumentException("Name can not be empty.");
            if (name.Any(c => char.IsWhiteSpace(c)))
                throw new ArgumentException("Name can not contain whitespace.");
            if (instanceRetrievalMethod != null && !typeof(InstanceRetrievalMethod).IsAssignableFrom(instanceRetrievalMethod))
                throw new ArgumentException("Passed InstanceRetrievalMethod Type does not inherit from UConsole.InstanceRetrievalMethod.");

            CommandName = name;
			CommandDescription = description;
            InstanceRetrievalMethod = instanceRetrievalMethod;
		}
    }
}
