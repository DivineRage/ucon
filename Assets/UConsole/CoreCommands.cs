﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UConsole;

static class CoreCommands {

    #region Application stuff
    [ConsoleCommand("quit", "Close the game.")]
    [ConsoleCommand("close", "Close the game.")]
    [ConsoleCommand("exit", "Close the game.")]
    static void Quit() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    [ConsoleCommand("load", "Load a level by the provided name. Optionally additive.")]
    static void LoadLevel(string name, bool additive = false) {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name, additive ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
    [ConsoleCommand("unload", "Unload a level by the provided name.")]
    static void UnloadLevel(string name) {
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(name);
    }

    [ConsoleCommand("screenshot", "Take a screenshot. When not provided a name, will default to \"screenshot_[NUM].png\"")]
    static void TakeScreenshot(string name = null, int supersize = 1) {
        if (name == null) {
            name = "screenshot";
            if (File.Exists(name + ".png")) {
                var count = 1;
                while (File.Exists(string.Format("{0}_{1}.png", name, count))) {
                    count++;
                }
                name = string.Format("{0}_{1}", name, count);
            }
        }
        supersize = Mathf.Max(1, supersize);
        ScreenCapture.CaptureScreenshot(name + ".png", supersize);

        Debug.LogFormat("Screenshot captured! Find it at \"{0}\"", Path.GetFullPath(name + ".png"));
    }

    [ConsoleCommand("savepp", "Save PlayerPrefs")]
    static void SavePlayerPrefs() {
        PlayerPrefs.Save();
    }
    #endregion

    #region Rendering

    [ConsoleCommand("maxfps", "Set the framerate cap.")]
    [ConsoleCommand("fpsmax", "Set the framerate cap.")]
    static int MaxFPS {
        get { return Application.targetFrameRate; }
        set { Application.targetFrameRate = Mathf.Max(-1, value); }
    }

    [ConsoleCommand("vsync", "Set the number of VSyncs that should pass between each frame.")]
    static int VSync { get { return QualitySettings.vSyncCount; } set { QualitySettings.vSyncCount = Mathf.Clamp(value, 0, 2); } }
    #endregion


    [ConsoleCommand("echo", "Logs the input text.")]
    static void Echo(string text) {
        Debug.Log(text);
    }



    // exit
    // close
    // quit
    // -- Closes the game. Exits playmode
    // load [name] [additive]
    // -- Loads a level with the given name
    // unload [name]
    // -- Unloads a level with the given name
    // fps_max [number]
    // -- Sets maxfps
    // screenshot
    // -- Takes a screenshot. Naming tbd

    // clear
    // -- Clears console log
    // echo [text]
    // -- Print text to console.

    // alias [commandName] [alias]
    // -- Binds an alias command
    // unalias [commandName]
    // -- Unbinds an alias command

    // savepp
    // -- Save PlayerPrefs

    // pause
    // resume
    // time                     -- Prints Time.time
    // realtimesincestartup     -- Prints Time.realTimeSinceStartup
    // timescale                -- Changes timescale
    // hierarchy                -- Prints hierarchy
    // fov [number]
    // -- Sets FOV of Camera.mainCamera

    // find                     -- Returns the first GameObject with that name
    // child [transform] [index]
    // child [transform] [name]
    // -- Gets the child for a particular transform. Will int.TryParse to get child index, otherwise name
    // getcomp/getcomponent     -- Returns the first Component of that type on that GameObject
    // ray [item]
    // -- Prints all physics objects along a ray defined by mouse position. Or #[item] along ray.

}
