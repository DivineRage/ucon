﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterChar : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(char); } }

        public override object Convert(string argument) {
            return char.Parse(argument);
        }

    }
}