﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterSByte : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(sbyte); } }

        public override object Convert(string argument) {
            return sbyte.Parse(argument);
        }

    }
}