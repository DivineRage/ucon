﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterInt16 : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(short); } }

        public override object Convert(string argument) {
            return short.Parse(argument);
        }

    }
}