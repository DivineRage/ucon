﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterUInt : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(uint); } }

        public override object Convert(string argument) {
            return uint.Parse(argument);
        }

    }
}