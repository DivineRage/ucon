﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterInt64 : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(long); } }

        public override object Convert(string argument) {
            return long.Parse(argument);
        }

    }
}