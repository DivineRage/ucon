﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterDouble : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(double); } }

        public override object Convert(string argument) {
            return double.Parse(argument);
        }

    }
}


