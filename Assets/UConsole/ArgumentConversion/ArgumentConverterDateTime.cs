﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterDateTime : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(DateTime); } }

        public override object Convert(string argument) {
            return DateTime.Parse(argument);
        }

    }
}