﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterStringArray : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(string[]); } }

        public override object Convert(string argument) {
            return UCon.ProcessInput(argument).SelectMany(cmd => cmd.AllArgumentsIncludingCommand.Select(arg => arg.AsString)).ToArray();
        }

    }
}