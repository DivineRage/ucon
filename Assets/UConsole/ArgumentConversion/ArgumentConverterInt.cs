﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterInt : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(int); } }

        public override object Convert(string argument) {
            return int.Parse(argument);
        }

    }
}