﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterUInt16 : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(ushort); } }

        public override object Convert(string argument) {
            return ushort.Parse(argument);
        }

    }
}