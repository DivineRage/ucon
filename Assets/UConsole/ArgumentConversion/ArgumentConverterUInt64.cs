﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterUInt64 : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(ulong); } }

        public override object Convert(string argument) {
            return ulong.Parse(argument);
        }

    }
}