﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterDecimal : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(decimal); } }

        public override object Convert(string argument) {
            return decimal.Parse(argument);
        }

    }
}