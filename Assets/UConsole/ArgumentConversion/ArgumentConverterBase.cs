﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public abstract class ArgumentConverterBase {

        public abstract Type ConvertedType { get; }

        public virtual string UsageDescription { get { return ""; } }

        public abstract object Convert(string argument);

    }
}
