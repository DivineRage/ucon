﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterBoolean : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(bool); } }

        public override object Convert(string argument) {
            argument = argument.ToLowerInvariant();
            if (argument == "1" ||
                argument == "true" ||
                argument == "t" ||
                argument == "yes" ||
                argument == "y") {
                return true;
            }
            else if (
              argument == "0" ||
              argument == "false" ||
              argument == "f" ||
              argument == "no" ||
              argument == "n") {
                return false;
            }

            return bool.Parse(argument);
        }

    }
}


