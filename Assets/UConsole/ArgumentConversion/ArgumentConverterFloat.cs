﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.ArgumentConversion {
    public class ArgumentConverterFloat : ArgumentConverterBase {

        public override Type ConvertedType { get { return typeof(float); } }

        public override object Convert(string argument) {
            return float.Parse(argument);
        }

    }
}