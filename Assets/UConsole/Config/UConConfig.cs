﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

namespace UConsole.Config {
    public class UConConfig {

        private static UConConfig _instance;
        public static UConConfig Instance { get { return _instance; } }

        // Which assemblies should be ignored while parsing? These should be any assembly that you are confident will not contain commands.
        public string[] assembliesToIgnore = new string[0];
        // Should assemblies be parsed asynchronously? This will speed up parsing, but not every command may be available immediately.
        public bool asyncParsing = true;
        // Allow multiple commands with the same name. This can lead to guesswork about which one to call.
        public bool allowCommandOverloading = false;



        #region Suggestions
        public bool enableSuggestions = true;
        #endregion

        #region Serialization
        public static void Save(string path) {
            using(var stream = new FileStream(path, FileMode.OpenOrCreate)) {
                using(var writer = new StreamWriter(stream, new UTF8Encoding())) {
                    var serializer = new XmlSerializer(typeof(UConConfig));
                    serializer.Serialize(writer, _instance);
                }
            }
        }

        public static void Load(string path) {
            using (var stream = new FileStream(path, FileMode.Open)) {
                using (var writer = new StreamReader(stream, new UTF8Encoding())) {
                    var serializer = new XmlSerializer(typeof(UConConfig));
                    _instance = (UConConfig)serializer.Deserialize(writer);
                }
            }
        }
        #endregion

    }
}