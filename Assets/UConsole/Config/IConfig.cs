﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Config {
	public interface IConfig {

        bool UseParsingWhitelist { get; }
        string[] ParsingWhitelist { get; }
        string[] ParsingBlacklist { get; }

        bool LogShowTimestamp { get; set; }
        bool LogShowSeverity { get; set; }

    }
}