﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UConsole.ArgumentConversion;
using UConsole.CommandEntries;
using UConsole.InstanceRetrieval;
using UnityEngine;

namespace UConsole {
    public static class UCon {

        private static VisibilityState _visState = VisibilityState.Large;
        public static VisibilityState CurrentVisibilityState {
            get { return _visState; }
            set {
                if (_visState == value)
                    return;
                _visState = value;
                OnVisibilityStateChanged(value);
            }
        }
        public static event Action<VisibilityState> OnVisibilityStateChanged = (v) => { };

        #region InstanceRetrievalMethodCache
        private static readonly Dictionary<Type, InstanceRetrievalMethod> _retrievalMethodCache = new Dictionary<Type, InstanceRetrievalMethod>();

        public static InstanceRetrievalMethod GetInstanceRetrievalMethod(Type retrievalMethodType) {
            if (_retrievalMethodCache.ContainsKey(retrievalMethodType))
                return _retrievalMethodCache[retrievalMethodType];

            if (typeof(InstanceRetrievalMethod).IsAssignableFrom(retrievalMethodType) && retrievalMethodType != typeof(InstanceRetrievalMethod)) {
                var instance = (InstanceRetrievalMethod)Activator.CreateInstance(retrievalMethodType);
                _retrievalMethodCache.Add(retrievalMethodType, instance);
                return instance;
            }

            throw new ArgumentException("Attempted to retrieve InstanceRetrievalMethod instance with an invalid type.");
        }
        #endregion

        #region Argument Conversion
        private static readonly Dictionary<Type, ArgumentConverterBase> _argumentConverterCache = new Dictionary<Type, ArgumentConverterBase>();

        public static bool HasArgumentConverter(Type desiredType) {
            return _argumentConverterCache.ContainsKey(desiredType);
        }

        public static ArgumentConverterBase GetArgumentConverter(Type desiredType) {
            if (_argumentConverterCache.ContainsKey(desiredType))
                return _argumentConverterCache[desiredType];

            throw new ArgumentException("Attempted to retrieve non-existant ArgumentConverter.");
        }
        #endregion

        #region Commands
        private static readonly Dictionary<string, CommandEntryBase> _commands = new Dictionary<string, CommandEntryBase>();

        public static object Execute(string command) {
            Debug.Log(command);

            var exState = new ExecutionState();
            exState.originalInput = command;

            ProcessInput(exState, command);

            return exState.Execute();
        }

        public static void ProcessInput(ExecutionState exState, string input) {
            exState.commands = ProcessInput(input);
        }
        public static List<ExecutionCommand> ProcessInput(string input) {
            if (input == null)
                throw new ArgumentNullException("input");

            input = input.Trim();
            if (input.Length == 0)
                return null;

            var cmds = new List<ExecutionCommand>();
            var curCommand = new ExecutionCommand();
            cmds.Add(curCommand);

            var curArg = new ExecutionArgument();
            curCommand.AddArgument(curArg);

            var singleQuoteActive = false;
            var doubleQuoteActive = false;
            var escapeActive = false;

            var ll = new List<char>(input);
            var curIndex = 0;

            // Inner function to make a token from the current list head.
            Action<Func<string, IExecutionToken>, Action, Action, Action> makeToken = (func, OnIf, OnElse, OnComplete) => {
                if (curIndex == 0) {
                    ll.RemoveAt(0);
                    if (OnIf != null)
                        OnIf();
                }
                else {
                    var str = new string(ll.Take(curIndex).ToArray());
                    ll.RemoveRange(0, curIndex + 1);
                    curArg.AddToken(func(str));
                    if (OnElse != null)
                        OnElse();
                }
                if (OnComplete != null)
                    OnComplete();
                curIndex = -1;
            };

            #region Process Quotes and Semicolons
            for (curIndex = 0; curIndex < ll.Count; curIndex++) {
                var nextIndex = curIndex + 1;
                var cur = ll[curIndex];
                var next = (nextIndex < ll.Count) ? new char?(ll[nextIndex]) : null;

                if (singleQuoteActive) {
                    if (cur == '\'') {
                        makeToken(str => new ExecutionTokenString(str, QuoteStateType.SingleQuote), null, null,
                            () => singleQuoteActive = false);
                    }
                }
                else if (escapeActive) {
                    escapeActive = false;
                }
                else if (doubleQuoteActive) {
                    //if (cur == '\'') {
                    //    makeToken(str => new ExecutionTokenString(str, QuoteStateType.DoubleQuote), null, null,
                    //        () => escapeActive = true);
                    //} else
                    if (cur == '"') {
                        makeToken(str => new ExecutionTokenString(str, QuoteStateType.DoubleQuote), null, null,
                            () => doubleQuoteActive = false);
                    }
                }
                else {
                    if (cur == '\'') {
                        makeToken(str => new ExecutionTokenString(str), null, null,
                            () => singleQuoteActive = true);
                    }
                    else if (cur == '"') {
                        makeToken(str => new ExecutionTokenString(str), null, null,
                            () => doubleQuoteActive = true);
                    }
                    else if (cur == '\\') {
                        makeToken(str => new ExecutionTokenString(str), null, null,
                            () => escapeActive = true);
                    }
                    else if (cur == ';') {
                        makeToken(str => new ExecutionTokenString(str),
                            null,
                            () => {
                                curCommand = new ExecutionCommand();
                                cmds.Add(curCommand);
                                curArg = new ExecutionArgument();
                                curCommand.AddArgument(curArg);
                            },
                            null);
                    }
                    else if (char.IsWhiteSpace(cur)) {
                        makeToken(str => new ExecutionTokenString(str),
                            () => {
                                if (curArg.TokenCount > 0) {
                                    curArg = new ExecutionArgument();
                                    curCommand.AddArgument(curArg);
                                }
                            },
                            () => {
                                curArg = new ExecutionArgument();
                                curCommand.AddArgument(curArg);
                            },
                            null);
                    }
                }

                if (next == null && ll.Count > 0) {
                    var str = new string(ll.ToArray());
                    curArg.AddToken(new ExecutionTokenString(str));
                }
            }
            #endregion

            #region Process
            foreach(var cmd in cmds) {
                foreach(var arg in cmd.AllArgumentsIncludingCommand) {
                    var tokens = arg._tokens;
                    for (int iToken = 0; iToken < tokens.Count; iToken++) {
                        var token = tokens[iToken];
                        if (token == null) {
                            throw new Exception();
                        } else if (token.GetType() != typeof(ExecutionTokenString)) {
                            throw new Exception();
                        } else {
                            var strToken = (ExecutionTokenString)token;
                            if (strToken.quoteState == QuoteStateType.DoubleQuote) {

                            } else if (strToken.quoteState == QuoteStateType.NoQuote) {
                                
                            }
                        }
                    }
                }
            }
            #endregion

            return cmds;
        }

        public static IEnumerable<CommandEntryBase> GetCommandsStartingWith(string command) {
            command = command.Trim().ToLowerInvariant();
            return _commands.Values.Where((c) => c.CommandName.ToLowerInvariant().StartsWith(command));
        }
        #endregion

        #region Parsing
        private static readonly List<Assembly> _parsedAssemblies = new List<Assembly>();

        public static void ParseAssemblies() {
            var assemblies = GetAssembliesToParse();
            foreach (var assembly in assemblies) {
                ParseAssembly(assembly);
            }
        }
        public static void ParseAssembliesAsync() {
            var assemblies = GetAssembliesToParse().ToArray();
            foreach (var assembly in assemblies) {
                ThreadPool.QueueUserWorkItem(new WaitCallback(ParseAssembly), assembly);
            }
        }

        private static void ParseAssembly(object asm) {
            try {
                Assembly assembly = (Assembly)asm;

                if (_parsedAssemblies.Contains(assembly)) return;
                else _parsedAssemblies.Add(assembly);

                foreach (var type in assembly.GetTypes()) {
                    ParseType(type);
                }
            }
            catch (Exception e) {
                Debug.LogException(e);
            }
        }
        private static void ParseType(Type t) {
            if (typeof(ArgumentConverterBase).IsAssignableFrom(t) && t != typeof(ArgumentConverterBase)) { // Is argumentConverter type.
                var instance = (ArgumentConverterBase)Activator.CreateInstance(t);
                if (_argumentConverterCache.ContainsKey(instance.ConvertedType)) {
                    // Already exists.
                }
                else {
                    _argumentConverterCache.Add(instance.ConvertedType, instance);
                    //LogFormat("Adding ArgumentConverter for type: {0}", instance.ConvertedType.FullName);
                }
                return;
            }

            var fields = t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            foreach (var field in fields) {
                ParseField(field);
            }

            var properties = t.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            foreach (var property in properties) {
                ParseProperty(property);
            }

            var events = t.GetEvents(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            foreach (var evt in events) {
                ParseEvent(evt);
            }

            var methods = t.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            foreach (var method in methods) {
                ParseMethod(method);
            }
        }

        private static void ParseField(FieldInfo field) {
            var attributes = field.GetCustomAttributes(typeof(ConsoleCommandAttribute), true);
            if (attributes.Length == 0)
                return;

            for (var i = 0; i < attributes.Length; i++) {
                var attr = (ConsoleCommandAttribute)attributes[i];
                if (!field.IsStatic && attr.InstanceRetrievalMethod == null) {
                    LogWarningFormat("Ignoring command \"{0}\" because it has no InstanceRetrievalMethod defined when it is non-static.", attr.CommandName);
                    continue;
                }
                if (attr.InstanceRetrievalMethod == typeof(InstanceRetrievalMethod)) {
                    LogWarningFormat("Ignoring command \"{0}\" because it uses the base InstanceRetrievalMethod class. Please use a subclass that actually implements behaviour.", attr.CommandName);
                    continue;
                }
                var cmd = new CommandEntryField(attr, field);
                _commands.Add(attr.CommandName, cmd);
                //Debug.LogFormat("ParseField has typeof(ConsoleCommandAttribute) attributes: {0}", field.Name);
            }
        }
        private static void ParseProperty(PropertyInfo property) {
            var attributes = property.GetCustomAttributes(typeof(ConsoleCommandAttribute), true);
            if (attributes.Length == 0)
                return;

            for (var i = 0; i < attributes.Length; i++) {
                var attr = (ConsoleCommandAttribute)attributes[i];
                if (!(property.CanRead ? property.GetGetMethod(true) : property.GetSetMethod(true)).IsStatic && attr.InstanceRetrievalMethod == null) {
                    LogWarningFormat("Ignoring command \"{0}\" because it has no InstanceRetrievalMethod defined when it is non-static.", attr.CommandName);
                    continue;
                }
                if (attr.InstanceRetrievalMethod == typeof(InstanceRetrievalMethod)) {
                    LogWarningFormat("Ignoring command \"{0}\" because it uses the base InstanceRetrievalMethod class. Please use a subclass that actually implements behaviour.", attr.CommandName);
                    continue;
                }
                var cmd = new CommandEntryProperty(attr, property);
                _commands.Add(attr.CommandName, cmd);
                //Debug.LogFormat("ParseProperty has typeof(ConsoleCommandAttribute) attributes: {0}", property.Name);
            }
        }
        private static void ParseEvent(EventInfo evt) {
            var attributes = evt.GetCustomAttributes(typeof(ConsoleCommandAttribute), true);
            if (attributes.Length == 0)
                return;

            for (var i = 0; i < attributes.Length; i++) {
                var attr = (ConsoleCommandAttribute)attributes[i];
                if (!evt.GetAddMethod(true).IsStatic && attr.InstanceRetrievalMethod == null) {
                    LogWarningFormat("Ignoring command \"{0}\" because it has no InstanceRetrievalMethod defined when it is non-static.", attr.CommandName);
                    continue;
                }
                if (attr.InstanceRetrievalMethod == typeof(InstanceRetrievalMethod)) {
                    LogWarningFormat("Ignoring command \"{0}\" because it uses the base InstanceRetrievalMethod class. Please use a subclass that actually implements behaviour.", attr.CommandName);
                    continue;
                }
                var cmd = new CommandEntryEvent(attr, evt);
                _commands.Add(attr.CommandName, cmd);
                //Debug.LogFormat("ParseEvent has typeof(ConsoleCommandAttribute) attributes: {0}", evt.Name);
            }
        }
        private static void ParseMethod(MethodInfo method) {
            var attributes = method.GetCustomAttributes(typeof(ConsoleCommandAttribute), true);
            if (attributes.Length == 0)
                return;

            // Assert no out parameters
            if (method.GetParameters().Any(pi => pi.IsOut)) {
                LogWarningFormat("Ignoring commands for method \"{0}\" because the method contains out parameters, these are not supported.", method.Name);
                return;
            }

            for (var i = 0; i < attributes.Length; i++) {
                var attr = (ConsoleCommandAttribute)attributes[i];

                // Does it need an InstanceRetrievalMethod?
                if ((!method.IsStatic || method.GetCustomAttributes(typeof(ExtensionAttribute), true).Length > 0) && attr.InstanceRetrievalMethod == null) {
                    LogWarningFormat("Ignoring command \"{0}\" because it has no InstanceRetrievalMethod defined when it is either non-static or an extension method.", attr.CommandName);
                    continue;
                }

                // If it has one but it is the base type?
                if (attr.InstanceRetrievalMethod == typeof(InstanceRetrievalMethod)) {
                    LogWarningFormat("Ignoring command \"{0}\" because it uses the base InstanceRetrievalMethod class. Please use a subclass that actually implements behaviour.", attr.CommandName);
                    continue;
                }

                var cmd = new CommandEntryMethod(attr, method);
                _commands.Add(attr.CommandName, cmd);
            }
        }

        private static IEnumerable<Assembly> GetAssembliesToParse() {
            return AppDomain.CurrentDomain.GetAssemblies().Where(ShouldParseAssembly);
        }
        private static bool ShouldParseAssembly(Assembly asm) {
            var asmName = asm.GetName().Name;
            switch (asmName) { // Add any extra assemblies here that you wish to ignore. 
                case "AssetStoreTools":
                case "AssetStoreToolsExtra":
                case "Boo.Lang":
                case "Boo.Lang.Compiler":
                case "Boo.Lang.Parser":
                case "I18N":
                case "I18N.West":
                case "ICSharpCode.NRefactory":
                case "Mono.Cecil":
                case "Mono.Security":
                case "SyntaxTree.Mono.Cecil":
                case "SyntaxTree.VisualStudio.Unity.Bridge":
                case "SyntaxTree.VisualStudio.Unity.Messaging":
                case "System":
                case "System.Configuration":
                case "System.Core":
                case "System.Data":
                case "System.Drawing":
                case "System.EnterpriseServices":
                case "System.Transactions":
                case "System.Web":
                case "System.Web.Services":
                case "System.Xml":
                case "System.Xml.Linq":
                case "Unity.DataContract":
                case "Unity.IvyParser":
                case "Unity.Locator":
                case "Unity.PackageManager":
                case "Unity.SerializationLogic":
                case "UnityEditor":
                case "UnityEditor.Advertisements":
                case "UnityEditor.Analytics":
                case "UnityEditor.Android.Extensions":
                case "UnityEditor.BB10.Extensions":
                case "UnityEditor.Graphs":
                case "UnityEditor.HoloLens":
                case "UnityEditor.LinuxStandalone.Extensions":
                case "UnityEditor.Metro.Extensions":
                case "UnityEditor.Networking":
                case "UnityEditor.OSXStandalone.Extensions":
                case "UnityEditor.Purchasing":
                case "UnityEditor.TestRunner":
                case "UnityEditor.TreeEditor":
                case "UnityEditor.UI":
                case "UnityEditor.VR":
                case "UnityEditor.WP8.Extensions":
                case "UnityEditor.WebGL.Extensions":
                case "UnityEditor.WindowsStandalone.Extensions":
                case "UnityEditor.iOS.Extensions":
                case "UnityEditor.iOS.Extensions.Common":
                case "UnityEditor.iOS.Extensions.Xcode":
                case "UnityEngine":
                case "UnityEngine.Analytics":
                case "UnityEngine.HoloLens":
                case "UnityEngine.Networking":
                case "UnityEngine.TestRunner":
                case "UnityEngine.UI":
                case "UnityEngine.VR":
                case "UnityScript":
                case "UnityScript.Lang":
                case "UnityVS.VersionSpecific":
                case "mscorlib":
                case "nunit.framework":
                    return false;

                default:
                    return true;
            }
        }
        #endregion

        #region Static Initialization
        static UCon() {
            // Load here.

            var instance = new ArgumentConverterStringArray();
            _argumentConverterCache.Add(instance.ConvertedType, instance);
        }
        #endregion

        #region Logging
        internal static void LogFormat(string s, params object[] args) {
            Debug.LogFormat("[UCon] " + s, args);
        }
        internal static void LogWarningFormat(string s, params object[] args) {
            Debug.LogWarningFormat("[UCon] " + s, args);
        }
        internal static void LogErrorFormat(string s, params object[] args) {
            Debug.LogErrorFormat("[UCon] " + s, args);
        }
        internal static void LogException(Exception e) {
            Debug.LogException(e);
        }
        #endregion

        #region Built-in commands
        [ConsoleCommand("list", "List all available commands and aliases.")]
        public static void ListCommands() {
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("All available commands:");
            var keys = _commands.Keys.ToList();
            for (var i = 0; i < keys.Count; i++) {
                sb.AppendLine(keys[i]);
            }
            Debug.Log(sb.ToString());
        }

        [ConsoleCommand("alias", "Add an alias command.")]
        public static void AddAlias(string alias, params string[] command) {
            if (alias == null || string.IsNullOrEmpty(alias = alias.Trim())) {
                Debug.LogError("Unable to add alias - It needs a name!");
                return;
            }

            if (command == null || command.Length == 0) {
                Debug.LogError("Unable to add alias - It needs a command to execute!");
                return;
            }

            alias = alias.Trim();
            if (_commands.ContainsKey(alias)) {
                var cmd = _commands[alias];
                if (cmd is CommandEntryAlias) {
                    _commands.Remove(alias);
                }
                else {
                    Debug.LogError("Unable to add alias - Command already exists with that name.");
                    return;
                }
            }

            var commandString = command[0];
            for (var i = 1; i < command.Length; i++)
                commandString += string.Format(" {0}", command[i]);

            var entry = new CommandEntryAlias(alias, commandString);
            _commands.Add(alias, entry);
            Debug.LogFormat("Added alias: {0}\n{1}", alias, commandString);
        }

        [ConsoleCommand("unalias", "Remove an existing alias command.")]
        public static void RemoveAlias(string alias) {
            alias = alias.Trim();
            if (_commands.ContainsKey(alias)) {
                var cmd = _commands[alias];
                if (cmd is CommandEntryAlias) {
                    _commands.Remove(alias);
                }
            }
        }
        #endregion

        #region Enums
        public enum CommandExecutionResult {
            Unknown,
            Success,
            ThrewException,
            NoTarget,
            TooManyTargets,
            FailedToParseArguments,
            TooFewArguments,
        }
        public enum VisibilityState {
            Hidden,
            Small,
            Large,
        }
        public enum QuoteStateType {
            NoQuote,
            DoubleQuote,
            SingleQuote,
        }
        #endregion

        #region Internal Types
        public class ExecutionState {

            public string originalInput;

            internal List<ExecutionCommand> commands = new List<ExecutionCommand>();



            public void AddCommand(ExecutionCommand curCommand) {
                commands.Add(curCommand);
            }

            public object Execute() {
                object result = null;
                for (int i = 0; i < commands.Count; i++) {
                    result = commands[i].Execute();
                }
                return result;
            }
        }

        public class ExecutionCommand : IExecutionToken {

            internal readonly List<ExecutionArgument> arguments = new List<ExecutionArgument>();

            private ExecutionArgument _command = null;
            private int _queueIndex = 0;

            public bool IsPossiblyTargetType {
                get {
                    return true;
                }
            }
            public object AsObject {
                get {
                    throw new NotImplementedException();
                }
            }
            public string AsString {
                get {
                    var obj = AsObject;
                    return obj == null ? "" : obj.ToString();
                }
            }
            public string CommandName {
                get {
                    return _command != null ? _command.AsString : null;
                }
            }

            public int ArgumentCount { get { return arguments.Count; } }
            public int ArgumentsLeftToDequeue { get { return arguments.Count - _queueIndex; } }

            public List<ExecutionArgument> AllArgumentsIncludingCommand {
                get {
                    var result = new List<ExecutionArgument>(arguments.Count + 1);
                    result.Add(_command);
                    result.AddRange(arguments);
                    return result;
                }
            }


            internal void AddArgument(ExecutionArgument token) {
                if (_command == null)
                    _command = token;
                else
                    arguments.Add(token);
            }

            public object Execute() {
                object result = null;

                var cmdName = CommandName;
                if (cmdName == null)
                    return result;

                if (!_commands.ContainsKey(cmdName)) {
                    throw new Exception(string.Format("No command exists with the name {0}", cmdName));
                }
                var cmd = _commands[cmdName];
                var exResult = cmd.Invoke(this, out result);
                if (exResult == CommandExecutionResult.Success) {
                    return result;
                }
                else {
                    //throw new Exception(string.Format("Command failed to execute. {0}", exResult));
                }

                return null;
            }

            public ExecutionArgument Dequeue() {
                return _queueIndex >= arguments.Count ? null : arguments[_queueIndex++];
            }

        }

        public class ExecutionArgument {

            internal readonly List<IExecutionToken> _tokens = new List<IExecutionToken>();

            public bool IsPossiblyTargetType {
                get {
                    return _tokens.Count == 1 && _tokens[0] is ExecutionCommand;
                }
            }
            public object AsObject {
                get {
                    return _tokens.Count == 1 ?
                        _tokens[0].AsObject :
                        string.Concat(_tokens.Select(t => t.AsString));
                }
            }
            public string AsString {
                get {
                    return string.Concat(_tokens.Select(t => t.AsString).ToArray());
                }
            }
            public int TokenCount { get { return _tokens.Count; } }


            public void AddToken(IExecutionToken token) {
                _tokens.Add(token);
            }

        }

        public class ExecutionTokenString : IExecutionToken {

            public string value;
            public QuoteStateType quoteState;

            public bool IsPossiblyTargetType {
                get {
                    return false;
                }
            }
            public object AsObject { get { return value; } }
            public string AsString { get { return value; } }



            public ExecutionTokenString(string value, QuoteStateType quoteState = QuoteStateType.NoQuote) {
                this.value = value;
                this.quoteState = quoteState;
            }

            public override string ToString() {
                return string.Format("\"{0}\" ({1})", AsString, quoteState);
            }

        }

        public interface IExecutionToken {

            bool IsPossiblyTargetType { get; }
            object AsObject { get; }
            string AsString { get; }

        }
        #endregion

    }
}
