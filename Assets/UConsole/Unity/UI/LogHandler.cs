﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
	public class LogHandler : MonoBehaviour {


        [SerializeField]
        private UConUI _uiCore;
        [SerializeField]
        private RectTransform _elementParent;
        [SerializeField]
        private LogItemElement _elementPrefab;

        private readonly LinkedList<LogItem> _logItems = new LinkedList<LogItem>();
        private readonly LinkedList<LogItemElement> _elements = new LinkedList<LogItemElement>();



        private void Awake() {
            if (_uiCore == null)
                _uiCore = GetComponentInParent<UConUI>();

            for (var i = 0; i < _elementParent.childCount; i++) {
                Destroy(_elementParent.GetChild(i).gameObject);
            }

            Application.logMessageReceived += Application_logMessageReceived;
        }

        private void Application_logMessageReceived(string condition, string stackTrace, LogType type) {
            var item = new LogItem(condition, stackTrace, type);
            _logItems.AddFirst(item);

            var instance = Instantiate(_elementPrefab, _elementParent);
            instance.Populate(item);
            _elements.AddFirst(instance);

            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)instance.transform);
        }

        [ConsoleCommand("clear", "Clears the log history visible in the console.", typeof(InstanceRetrieval.InstanceRetrievalAllObjects))]
        public void Clear() {
            while (_elements.Count > 0) {
                Destroy(_elements.First.Value.gameObject);
                _elements.RemoveFirst();
            }
        }

    }
}