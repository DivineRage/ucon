﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
	public class LogItemElement : MonoBehaviour {

        [SerializeField]
        private Text _labelTimestamp;

        [SerializeField]
        private Text _labelLogType;

        [SerializeField]
        private Text _labelMessage;

        private LogItem _item;



        public void Populate(LogItem item) {
            _item = item;
            _labelMessage.text = ColorText(GetColorForLogType(_item.Type), _item.Message);
            _labelLogType.text = ColorText(GetColorForLogType(_item.Type), _item.Type.ToString());
            var t = System.TimeSpan.FromSeconds(_item.Timestamp);
            _labelTimestamp.text = string.Format("[{0:D2}:{1:D2}:{2:D2}:{3:D3}]",
                t.Hours,
                t.Minutes,
                t.Seconds,
                t.Milliseconds);
        }

        private Color32 GetColorForLogType(LogType type) {
            switch (type) {
                default:
                case LogType.Log:
                    return Color.white;

                case LogType.Warning:
                    return new Color32(255, 231, 49, 255);

                case LogType.Error:
                    return new Color32(255, 60, 60, 255);

                case LogType.Exception:
                    return new Color32(255, 60, 60, 255);

                case LogType.Assert:
                    return new Color32(0, 160, 255, 255);
            }
        }

        private static string ColorText(Color32 color, string message) {
            return string.Format("<color=#{0:X2}{1:X2}{2:X2}{3:X2}>{4}</color>", color.r, color.g, color.b, color.a, message);
        }
        private static string ColorText(string colorString, string message) {
            return string.Format("<color=#{0}>{1}</color>", colorString, message);
        }

    }
}