﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UConsole.CommandEntries;
using UnityEngine;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
    public class SuggestionElement : MonoBehaviour {

        [SerializeField]
        private RectTransform[] _signatureRects;
        [SerializeField]
        private RectTransform[] _usageRects;
        [SerializeField]
        private RectTransform[] _descriptionRects;
        [SerializeField]
        private RectTransform _selectionRect;

        [SerializeField]
        private Text _signatureText;
        [SerializeField]
        private Text _usageText;
        [SerializeField]
        private Text _descriptionText;

        private CommandEntryBase _entry;

        public Color colorSignatureProfile = Color.gray;
        public Color colorInstanceRetrievalArguments = Color.gray;
        public Color colorRegularArguments = Color.gray;
        public Color colorUsage = Color.gray;
        public Color colorDescription = Color.gray;



        public void Populate(CommandEntryBase entry) {
            _entry = entry;
            _signatureText.text = GetSignatureString();
            _usageText.text = GetUsageString();
            _descriptionText.text = GetDescriptionString();
        }

        public void SetExpansionMode(bool selected, ExpansionModeFlags state) {
            for (int i = 0; i < _signatureRects.Length; i++) {
                _signatureRects[i].gameObject.SetActive((state & ExpansionModeFlags.Signature) != 0);
            }
            for (int i = 0; i < _usageRects.Length; i++) {
                //_usageRects[i].gameObject.SetActive((state & ExpansionModeFlags.Usage) != 0);
                _usageRects[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < _descriptionRects.Length; i++) {
                _descriptionRects[i].gameObject.SetActive((state & ExpansionModeFlags.Description) != 0 && !string.IsNullOrEmpty(_entry.CommandDescription));
            }

            _selectionRect.gameObject.SetActive(selected);
        }


        private string GetSignatureString() {
            var result = new StringBuilder(string.Format("{0}{1}", GetSignatureTokens(), _entry.CommandName));

            // Add Instance Retrieval Arguments
            if (_entry.InstanceRetrievalMethodType != null) {
                var retrievalMethod = UCon.GetInstanceRetrievalMethod(_entry.InstanceRetrievalMethodType);
                if (retrievalMethod.RequiredArgumentCount > 0) {
                    result.AppendFormat("<color=#{0}><i>", ColorToHex(colorInstanceRetrievalArguments));
                    for (var i = 0; i < retrievalMethod.RequiredArgumentCount; i++) {
                        result.AppendFormat(" {0}", retrievalMethod.RequiredArgumentNames[i]);
                    }
                    result.Append("</i></color>");
                }
            }

            // Add Arguments.
            var expectedParameters = _entry.ExpectedParameters;
            if (expectedParameters.Length > 0) {
                result.AppendFormat("<color=#{0}>", ColorToHex(colorRegularArguments));
                for (var i = 0; i < expectedParameters.Length; i++) {
                    var param = expectedParameters[i];
                    if (param.IsParams) {
                        result.AppendFormat(" [{0} {1} ...]", param.Type.Name, param.ParameterName);
                    }
                    else if (param.IsOptional) {
                        result.AppendFormat(" [{0} {1} = {2}]", param.Type.Name, param.ParameterName, param.DefaultValue);
                    }
                    else {
                        result.AppendFormat(" [{0} {1}]", param.Type.Name, param.ParameterName);
                    }
                }
                result.Append("</color>");
            }

            return result.ToString();
        }
        private string GetSignatureTokens() {
            if (_entry.CommandType == CommandEntryBase.CommandEntryType.Alias)
                return "[Alias] ";
            return string.Format("<color=#{0}>[{1}{2}{3}]</color> ", ColorToHex(colorSignatureProfile), GetAccessModifierToken(), _entry.IsStatic ? "S" : "", _entry.CommandType.ToString().Substring(0, 1));
        }
        private string GetAccessModifierToken() {
            var result = "";

            if ((_entry.AccessModifiers & CommandEntryBase.AccessModifierFlags.Public) != 0)
                result += "+";
            else if ((_entry.AccessModifiers & CommandEntryBase.AccessModifierFlags.Private) != 0)
                result += "-";
            else if ((_entry.AccessModifiers & CommandEntryBase.AccessModifierFlags.Protected) != 0)
                result += "#";
            else if ((_entry.AccessModifiers & CommandEntryBase.AccessModifierFlags.Internal) != 0)
                result += "|";

            return result;
        }

        private string GetUsageString() {
            return string.Format("<color=#{0}>{1}</color>", ColorToHex(colorUsage), "Undefined.");
        }

        private string GetDescriptionString() {
            return string.Format("<color=#{0}>{1}</color>", ColorToHex(colorDescription), _entry.CommandDescription);
        }


        private string ColorToHex(Color32 color) {
            return color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2") + color.a.ToString("X2");
        }



        [Flags]
        public enum ExpansionModeFlags {
            Signature = 1,
            Usage = 2,
            Description = 4,
        }

    }
}