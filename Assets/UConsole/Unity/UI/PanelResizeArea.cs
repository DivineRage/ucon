﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
    public class PanelResizeArea : MonoBehaviour, IPointerDownHandler, IDragHandler {

        public RectTransform minSizeRect;
        public RectTransform panelRectTransform;

        private Vector2 originalLocalPointerPosition;
        private Vector2 originalSizeDelta;

        public void OnPointerDown(PointerEventData eventData) {
            originalSizeDelta = panelRectTransform.sizeDelta;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelRectTransform, eventData.position, eventData.pressEventCamera, out originalLocalPointerPosition);
        }

        public void OnDrag(PointerEventData eventData) {
            if (panelRectTransform == null)
                return;

            Vector2 localPointerPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelRectTransform, eventData.position, eventData.pressEventCamera, out localPointerPosition);
            Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;

            Vector2 sizeDelta = originalSizeDelta + new Vector2(0f, -offsetToOriginal.y);
            sizeDelta = new Vector2(sizeDelta.x, Mathf.Clamp(sizeDelta.y, minSizeRect.sizeDelta.y, Screen.height - 8));

            panelRectTransform.sizeDelta = sizeDelta;
        }

    }
}