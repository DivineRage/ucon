﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Unity.UI {
	public class LogItem {

        public readonly string Message;
        public readonly string StackTrace;
        public readonly LogType Type;
        public readonly float Timestamp;

        public float ElementHeightCache { get; set; }



        public LogItem(string message, string stackTrace, LogType type) {
            Message = message;
            StackTrace = stackTrace;
            Type = type;
            Timestamp = Time.realtimeSinceStartup;
        }

	}
}