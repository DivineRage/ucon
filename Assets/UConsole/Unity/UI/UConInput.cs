﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
    public class UConInput : MonoBehaviour {

        public int CaretIndex { get { return _inputField.caretPosition; } set { _inputField.caretPosition = value; } }
        public bool IsFocused { get { return _inputField.isFocused; } }
        public bool IsInteractable { get { return _inputField.interactable; } set { _inputField.interactable = value; } }
        public string Text { get { return _inputField.text; } set { _inputField.text = value; } }

        [SerializeField]
        private UConUI _uiCore;
        [SerializeField]
        private InputField _inputField;

        private readonly List<string> _commandHistory = new List<string>();
        public int CommandHistoryCount { get { return _commandHistory.Count; } }



        private void Start() {
            if (_uiCore == null)
                _uiCore = GetComponentInParent<UConUI>();
        }

        public void OnInputChanged(string input) {
            if ((input = input.Trim()) == "")
                return;
            if (input == "`") {
                _inputField.text = "";
                return;
            }

            _uiCore.SuggestionsComponent.SuggestForInput(input, _uiCore.InputComponent.CaretIndex);
            if (_uiCore.HistoryIndex < 0)
                _uiCore.HistoryIndex = 0;

            var commands = UCon.GetCommandsStartingWith(input);
            if (commands.Count() == 0)
                return;

            var sb = new StringBuilder();
            foreach (var cmd in commands) {
                sb.AppendLine(cmd.CommandName);
            }
        }
        public void OnInputEnd(string input) {
            if (input == null || input.Trim().Length == 0)
                return;

            _commandHistory.Insert(0, input);
            UCon.Execute(input);
            Clear();
            Focus();
        }


        public void Clear() {
            _inputField.text = "";
            _uiCore.SuggestionsComponent.SuggestForInput("", 0);
        }

        public void CompleteWithString(string suggestion) {
            Text = suggestion + " ";
            CaretIndex = Text.Length;
        }

        public void Focus() {
            _inputField.interactable = true;
            _inputField.Select();
            _inputField.ActivateInputField();
        }
        public void Unfocus() {
            _inputField.DeactivateInputField();
        }

        /// <summary>
        /// Sets current text to history state at the given index. 0 for clearing to nothing.
        /// </summary>
        /// <param name="historyIndex">Point in history to go to.</param>
        /// <returns>The actual point in history it's using. If the desired point was too far back or in the future, will result whatever index is on the boundary.</returns>
        public void SetToHistoryState(int historyIndex) {
            _inputField.text = _commandHistory[historyIndex];
            CaretIndex = Text.Length;
        }

    }
}