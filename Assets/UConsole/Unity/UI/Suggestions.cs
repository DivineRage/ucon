﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UConsole.Unity.UI {
    public class Suggestions : MonoBehaviour {

        [SerializeField]
        private UConUI _uiCore;
        [SerializeField]
        private RectTransform _elementParent;
        [SerializeField]
        private SuggestionElement _elementPrefab;

        private readonly List<SuggestionElement> _elements = new List<SuggestionElement>();
        private List<CommandEntries.CommandEntryBase> _suggestions = new List<CommandEntries.CommandEntryBase>();

        private int _selectedSuggestionIndex = 0;
        public int SelectedSuggestionIndex {
            get {
                return _selectedSuggestionIndex;
            }
            set {
                _selectedSuggestionIndex = Mathf.Clamp(value, -1, _suggestions.Count - 1);

                if (_selectedSuggestionIndex == -1) {
                    _selectedSuggestionKey = "";
                }
                else {
                    _selectedSuggestionKey = _suggestions[_selectedSuggestionIndex].CommandName;
                }

                UpdateSuggestionElements(_selectedSuggestionIndex);
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
        }
        private string _selectedSuggestionKey = "";
        public string SelectedSuggestionKey { get { return _selectedSuggestionKey; } }


        private void Start() {
            if (_uiCore == null)
                _uiCore = GetComponentInParent<UConUI>();

            for (var i = 0; i < _elementParent.childCount; i++) {
                Destroy(_elementParent.GetChild(i).gameObject);
            }
        }

        public void SuggestForInput(string input, int caretPosition) {
            input = input.Trim();
            string command = input;
            for (var i = 0; i < input.Length; i++) {
                if (char.IsWhiteSpace(input, i)) {
                    command = input.Substring(0, i);
                    break;
                }
            }

            if (string.IsNullOrEmpty(input)) {
                for (var i = 0; i < _elements.Count; i++) {
                    var el = _elements[i];
                    el.gameObject.SetActive(false);
                }
                return;
            }

            _suggestions = UCon.GetCommandsStartingWith(command).ToList();

            // Make space for all elements, then create new ones if needed.
            _elements.Capacity = Mathf.Max(_elements.Capacity, _suggestions.Count);
            while (_elements.Count < _suggestions.Count) {
                _elements.Add(Instantiate(_elementPrefab, _elementParent));
            }

            // Find the previously selected suggestion, otherwise the first one in the list.
            SelectedSuggestionIndex = Mathf.Max(0, _suggestions.FindIndex(e => e.CommandName == _selectedSuggestionKey));

            UpdateSuggestionElements(SelectedSuggestionIndex);
        }

        private void UpdateSuggestionElements(int selectedElementIndex) {
            for (var i = 0; i < _elements.Count; i++) {
                var el = _elements[i];
                el.gameObject.SetActive(i < _suggestions.Count);

                if (i < _suggestions.Count) {
                    el.Populate(_suggestions[i]);

                    SuggestionElement.ExpansionModeFlags flags;
                    if (i == selectedElementIndex)
                        flags = SuggestionElement.ExpansionModeFlags.Signature | SuggestionElement.ExpansionModeFlags.Usage | SuggestionElement.ExpansionModeFlags.Description;
                    else
                        flags = SuggestionElement.ExpansionModeFlags.Signature;

                    el.SetExpansionMode(i == selectedElementIndex, flags);
                }
            }
        }

    }
}