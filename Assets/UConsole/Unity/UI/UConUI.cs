﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UConsole;
using UConsole.Config;
using System.Text;
using System.Linq;

namespace UConsole.Unity.UI {
    public class UConUI : MonoBehaviour {

        private static UConUI _instance;

        private static readonly Dictionary<string, string> _bindEntries = new Dictionary<string, string>();
        private static List<RaycastHit> _instanceRetrievalRaycastResults = null;
        public static List<RaycastHit> InstanceRetrievalRaycastResults {
            get {
                if (_instanceRetrievalRaycastResults == null)
                    RaycastForInstanceRetrieval();

                return _instanceRetrievalRaycastResults;
            }
        }

        [SerializeField]
        private GameObject _parent;

        [SerializeField]
        private LogHandler _logComponent;
        [SerializeField]
        private UConInput _inputComponent;
        [SerializeField]
        private PanelResizeArea _resizeArea;
        [SerializeField]
        private Suggestions _suggestionsComponent;


        public LogHandler LogComponent { get { return _logComponent; } }
        public UConInput InputComponent { get { return _inputComponent; } }
        public Suggestions SuggestionsComponent { get { return _suggestionsComponent; } }
        private RectTransform RT { get { return (RectTransform)transform; } }

        private int _historyIndex = 0;
        public int HistoryIndex {
            get { return _historyIndex; }
            set {
                var old = _historyIndex;
                _historyIndex = Mathf.Clamp(value, 0, InputComponent.CommandHistoryCount);
                if (_historyIndex == 0) {
                    if (old != 0) {
                        InputComponent.Text = "";
                        SuggestionsComponent.SuggestForInput(InputComponent.Text, InputComponent.CaretIndex);
                    }
                }
                else {
                    InputComponent.SetToHistoryState(_historyIndex - 1);
                    SuggestionsComponent.SuggestForInput(InputComponent.Text, InputComponent.CaretIndex);
                }
            }
        }



        void Awake() {
            if (_instance != null && _instance != this) {
                Destroy(transform.root.gameObject);
                return;
            } else {
                _instance = this;
                DontDestroyOnLoad(transform.root.gameObject);
            }
            UCon.OnVisibilityStateChanged += UCon_OnVisibilityStateChanged;
        }
        void Start() {
            UCon_OnVisibilityStateChanged(UCon.CurrentVisibilityState);
            UCon.ParseAssemblies();
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.BackQuote)) {
                switch (UCon.CurrentVisibilityState) {
                    case UCon.VisibilityState.Hidden:
                        UCon.CurrentVisibilityState = UCon.VisibilityState.Large;
                        break;
                    case UCon.VisibilityState.Small:
                        UCon.CurrentVisibilityState = UCon.VisibilityState.Large;
                        break;
                    case UCon.VisibilityState.Large:
                        UCon.CurrentVisibilityState = UCon.VisibilityState.Hidden;
                        break;
                }
            }

            if (UCon.CurrentVisibilityState != UCon.VisibilityState.Hidden &&
                Input.GetMouseButton(0)) {
                RaycastForInstanceRetrieval();
            }

            if (UCon.CurrentVisibilityState == UCon.VisibilityState.Hidden) {
                var keys = _bindEntries.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++) {
                    var key = keys[i];
                    if (Input.GetKeyDown(key)) {
                        UCon.Execute(_bindEntries[key]);
                    }
                }
            }
        }
        void LateUpdate() {
            if (!InputComponent.IsFocused)
                return;

            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                // Select suggestion above current one.
                HistoryIndex++;
                //if (SelectedElementIndex > 0) {
                //    SelectedElementIndex--;
                //    UpdateSuggestionElements(SelectedElementIndex);
                //    LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
                //}
            }

            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                // Select suggestion below current one.
                HistoryIndex--;
                //if (SelectedElementIndex < _suggestions.Count - 1) {
                //    SelectedElementIndex++;
                //    UpdateSuggestionElements(SelectedElementIndex);
                //    LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
                //}
            }

            if (Input.GetKeyDown(KeyCode.Tab)) {
                // Autocomplete if possible.
                InputComponent.CompleteWithString(SuggestionsComponent.SelectedSuggestionKey);
            }
        }

        private void UCon_OnVisibilityStateChanged(UCon.VisibilityState obj) {
            var sizeDelta = RT.sizeDelta;
            switch (obj) {
                case UCon.VisibilityState.Hidden:
                    _parent.SetActive(false);
                    _logComponent.gameObject.SetActive(false);
                    _resizeArea.gameObject.SetActive(false);
                    RT.sizeDelta = new Vector2(sizeDelta.x, ((RectTransform)_inputComponent.transform).sizeDelta.y);
                    _inputComponent.IsInteractable = false;
                    _inputComponent.Clear();
                    break;

                case UCon.VisibilityState.Small:
                    _parent.SetActive(true);
                    _logComponent.gameObject.SetActive(false);
                    _resizeArea.gameObject.SetActive(false);
                    RT.sizeDelta = new Vector2(sizeDelta.x, ((RectTransform)_inputComponent.transform).sizeDelta.y);
                    _inputComponent.Focus();
                    break;

                case UCon.VisibilityState.Large:
                    _parent.SetActive(true);
                    _logComponent.gameObject.SetActive(true);
                    _resizeArea.gameObject.SetActive(true);
                    RT.sizeDelta = new Vector2(sizeDelta.x, 132f);
                    _inputComponent.Focus();
                    break;
            }
        }



        [ConsoleCommand("bind", "Bind a custom command to any key being executed.")]
        private static void Bind(string keycode, string command = null) {
            if (keycode == null || string.IsNullOrEmpty(keycode = keycode.Trim())) {
                Debug.LogError("Could not bind command: no key name given.");
                return;
            }

            if (command != null) {
                if (_bindEntries.ContainsKey(keycode)) {
                    _bindEntries[keycode] = command;
                }
                else {
                    _bindEntries.Add(keycode, command);
                }
            }

            Debug.LogFormat("\"{0}\" - \"{1}\"", keycode, _bindEntries.ContainsKey(keycode) ? _bindEntries[keycode] : "");
        }

        public static void RaycastForInstanceRetrieval(Ray r) {
            var results = Physics.RaycastAll(r).ToList();
            results.Sort((rch1, rch2) => rch1.distance.CompareTo(rch2.distance));
            _instanceRetrievalRaycastResults = results;
        }
        public static void RaycastForInstanceRetrieval(Vector2 screenPosition) {
            RaycastForInstanceRetrieval(Camera.main.ScreenPointToRay(screenPosition));
        }
        public static void RaycastForInstanceRetrieval() {
            RaycastForInstanceRetrieval(Camera.main.ScreenPointToRay(Input.mousePosition));
        }

    }
}