﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Tests {
	public class CommandTestsInstanceFields : MonoBehaviour {

        [ConsoleCommand("TestFieldPublicInstInt", null, typeof(InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public int TestFieldPublicInstInt = 5;
        [ConsoleCommand("TestFieldPrivateInstInt", null, typeof(InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        private int TestFieldPrivateInstInt = 5;

        [ConsoleCommand("TestFieldPublicInstFloat", null, typeof(InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public float TestFieldPublicInstFloat = 5f;
        [ConsoleCommand("TestFieldPublicInstVector3", null, typeof(InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public Vector3 TestFieldPublicInstVector3 = Vector3.one * 5;

    }
}