﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Tests {
	public class CommandTestsStaticFields {

        [ConsoleCommand("TestFieldPublicStaticInt", null)]
        public static int TestFieldPublicStaticInt = 3;
        [ConsoleCommand("TestFieldPrivateStaticInt", null)]
        private static int TestFieldPrivateStaticInt = 3;

    }
}