﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Tests {
	public class CommandTestsInstanceMethods : MonoBehaviour {

        [ConsoleCommand("TestPublicInstMethodVoidVoid", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public void TestPublicInstMethodVoidVoid() {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, null);
        }
        [ConsoleCommand("TestPublicInstMethodVoidBool", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public void TestPublicInstMethodVoidBool(bool b) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, b);
        }
        [ConsoleCommand("TestPublicInstMethodVoidVector3", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public void TestPublicInstMethodVoidVector3(Vector3 v) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, v);
        }
        [ConsoleCommand("TestPublicInstMethodVoidComponent", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public void TestPublicInstMethodVoidComponent(MonoBehaviour mb) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, mb);
        }

        [ConsoleCommand("TestPublicInstMethodFloatVoid", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public float TestPublicInstMethodFloatVoid() {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, null); return 8f;
        }
        [ConsoleCommand("TestPublicInstMethodFloatBool", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public float TestPublicInstMethodFloatBool(bool b) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, b); return 8f;
        }
        [ConsoleCommand("TestPublicInstMethodFloatVector3", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public float TestPublicInstMethodFloatVector3(Vector3 v) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, v); return 8f;
        }
        [ConsoleCommand("TestPublicInstMethodFloatComponent", null, typeof(UConsole.InstanceRetrieval.InstanceRetrievalHierarchyFind))]
        public float TestPublicInstMethodFloatComponent(MonoBehaviour mb) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, mb); return 8f;
        }

    }
}