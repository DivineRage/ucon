﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.Tests {
    public static class CommandTestsStaticMethods {

        public static event Action OnVoidMethodCalled = () => {};

        [ConsoleCommand("TestMethodPublicStaticVoidVoid", null)]
        public static void TestMethodPublicStaticVoidVoid() {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, null);
            OnVoidMethodCalled();
        }

        [ConsoleCommand("TestMethodPublicStaticVoidFloat", null)]
        public static void TestMethodPublicStaticVoidFloat(float f) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, f);
            OnVoidMethodCalled();
        }

        [ConsoleCommand("TestMethodPublicStaticVoidFloatOptional", null)]
        public static void TestMethodPublicStaticVoidFloatOptional(float f = 1f) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, f);
            OnVoidMethodCalled();
        }

        [ConsoleCommand("TestMethodPublicStaticFloatVoid", "returns 1f")]
        public static float TestMethodPublicStaticFloatVoid() {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, null);
            OnVoidMethodCalled();
            return 1f;
        }

        [ConsoleCommand("TestMethodPublicStaticFloatFloatOptional", "returns 1f or whatever is passed in.")]
        public static float TestMethodPublicStaticFloatFloatOptional(float f = 1f) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, f);
            OnVoidMethodCalled();
            return f;
        }

        [ConsoleCommand("TestMethodPublicStaticFloatBool", null)]
        public static float TestMethodPublicStaticFloatBool(bool b) {
            Debug.LogFormat("[{0}] {1}", new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name, b);
            OnVoidMethodCalled();
            return b ? 2f : 1f;
        }

    }
}