﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Reflection;
using System.Linq;
using UConsole;
using System.Collections.Generic;

namespace UConsole.Tests {
    public class TestCommandTokenization {

        [Test]
        public void TestTokenize() {
            var command = "command test1 test2\" part 1 \"part2; command2 test3";
            Debug.Log(command);
            var result = (List<UCon.ExecutionCommand>)UCon.ProcessInput(command);
            Assert.NotNull(result);
            LogTokenizeResult(result);
        }
        [Test]
        public void TestTokenize2() {
            var command = "fire \"player 1\" 100";
            Debug.Log(command);
            var result = (List<UCon.ExecutionCommand>)UCon.ProcessInput(command);
            Assert.NotNull(result);
            LogTokenizeResult(result);
        }

        private void LogTokenizeResult(List<UCon.ExecutionCommand> result) {
            var tokensField = typeof(UCon.ExecutionArgument).GetField("_tokens", 
                BindingFlags.NonPublic | BindingFlags.Instance);

            for (int iCmd = 0; iCmd < result.Count; iCmd++) {
                var cmd = result[iCmd];
                var allArgs = cmd.AllArgumentsIncludingCommand;

                for (int iArg = 0; iArg < allArgs.Count; iArg++) {
                    var arg = allArgs[iArg];
                    var argTokens = (List<UCon.IExecutionToken>)tokensField.GetValue(arg);
                    var str = string.Join(", ", argTokens.Select(t => t.ToString()).ToArray());
                    Debug.LogFormat("c{0} a{1} > {2}", iCmd, iArg, str);
                    //for (int iTok = 0; iTok < arg.tokens.Count; iTok++) {
                    //    var token = arg.tokens[iTok];
                    //    Debug.LogFormat("c{0} a{1} t{2} > {3}", iCmd, iArg, iTok, token);
                    //}
                }
                Debug.Log("");
            }
        }


        /*
        [Test]
        public void TestTokenizeEmpty() {
            var result = UCon.TokenizeString("");
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 0);
        }

        [Test]
        public void TestTokenizeCommand() {
            var result = UCon.TokenizeString("command");
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("command", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSimpleArgument() {
            var result = UCon.TokenizeString("command test1");
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test1", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSimpleArguments() {
            var result = UCon.TokenizeString("command test1 test2");
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test1", result.Dequeue());
            Assert.AreEqual("test2", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSingleQuoteWithoutWhitespace() {
            var result = UCon.TokenizeString("command \"test\"");
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSingleQuoteWithWhitespace() {
            var result = UCon.TokenizeString("command \"test foo\"");
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test foo", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSingleQuoteSubquoteWithoutWhitespace() {
            var result = UCon.TokenizeString("command \"test\'test2\'\"");
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test\'test2\'", result.Dequeue());
        }

        [Test]
        public void TestTokenizeSingleQuoteSubquoteWithWhitespace() {
            var result = UCon.TokenizeString("command \"test \'test2\' foo\"");
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test \'test2\' foo", result.Dequeue());
        }

        [Test]
        public void TestTokenizeAdvancedArguments() {
            var result = UCon.TokenizeString("command test1 \"test2 foo\" \"test3 'subtest' bar\" 'asd \" fgh \" jkl' \"asd \" fgh \" jkl\"");
            Assert.IsNotNull(result);
            Assert.AreEqual(8, result.Count);
            Assert.AreEqual("command", result.Dequeue());
            Assert.AreEqual("test1", result.Dequeue());
            Assert.AreEqual("test2 foo", result.Dequeue());
            Assert.AreEqual("test3 'subtest' bar", result.Dequeue());
            Assert.AreEqual("asd \" fgh \" jkl", result.Dequeue());
            Assert.AreEqual("asd ", result.Dequeue());
            Assert.AreEqual("fgh", result.Dequeue());
            Assert.AreEqual(" jkl", result.Dequeue());
        }
        */
    }
}