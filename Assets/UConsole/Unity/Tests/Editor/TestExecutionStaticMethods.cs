﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System;
using System.Collections;

namespace UConsole.Tests {
    public class TestExecutionStaticMethods : IPrebuildSetup {

        public void Setup() {
            UCon.ParseAssemblies();
        }

        [Test]
        public void TestMethodVoidVoid() {
            UCon.ParseAssemblies();
            bool executed = false;
            Action del = () => { executed = true; };
            CommandTestsStaticMethods.OnVoidMethodCalled += del;
            var result = UCon.Execute("TestMethodPublicStaticVoidVoid");
            CommandTestsStaticMethods.OnVoidMethodCalled -= del;
            Assert.IsTrue(executed);
            Assert.IsNull(result);
        }

        [Test]
        public void TestMethodVoidFloatOptional() {
            UCon.ParseAssemblies();
            bool executed = false;
            Action del = () => { executed = true; };
            CommandTestsStaticMethods.OnVoidMethodCalled += del;
            var result = UCon.Execute("TestMethodPublicStaticVoidFloatOptional");
            CommandTestsStaticMethods.OnVoidMethodCalled -= del;
            Assert.IsTrue(executed);
            Assert.IsNull(result);

            executed = false;
            CommandTestsStaticMethods.OnVoidMethodCalled += del;
            result = UCon.Execute("TestMethodPublicStaticVoidFloatOptional 8");
            CommandTestsStaticMethods.OnVoidMethodCalled -= del;
            Assert.IsTrue(executed);
            Assert.IsNull(result);
        }

        [Test]
        public void TestMethodFloatFloatOptional() {
            UCon.ParseAssemblies();

            bool executed = false;
            Action del = () => { executed = true; };
            CommandTestsStaticMethods.OnVoidMethodCalled += del;
            var result = UCon.Execute("TestMethodPublicStaticFloatFloatOptional");
            CommandTestsStaticMethods.OnVoidMethodCalled -= del;
            Assert.IsTrue(executed);
            Assert.IsNotNull(result);
            Assert.AreEqual(1f, result);

            executed = false;
            CommandTestsStaticMethods.OnVoidMethodCalled += del;
            result = UCon.Execute("TestMethodPublicStaticFloatFloatOptional 2");
            CommandTestsStaticMethods.OnVoidMethodCalled -= del;
            Assert.IsTrue(executed);
            Assert.IsNotNull(result);
            Assert.AreEqual(2f, result);
        }
    }
}