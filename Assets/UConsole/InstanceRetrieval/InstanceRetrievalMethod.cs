﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.InstanceRetrieval {
    public abstract class InstanceRetrievalMethod {

        public abstract int RequiredArgumentCount { get; }
        public abstract string[] RequiredArgumentNames { get; }
        public abstract string[] RequiredArgumentUsageDescriptions { get; }

        public abstract object[] GetInvokeTarget(Type requiredType, string[] arguments);



        public enum RetrieveCount {
            None,
            Single,
            Multiple,
        }

    }
}