﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.InstanceRetrieval {
    public class InstanceRetrievalHierarchyFind : InstanceRetrievalMethod {

        public override int RequiredArgumentCount { get { return 1; } }
        // This exists purely so we can indicate the argument requirement to the user in the UI.
        public override string[] RequiredArgumentNames { get { return new string[] { "Name" }; } }
        public override string[] RequiredArgumentUsageDescriptions { get { return new string[] { "GameObject name to find Component on." }; } }

        public override object[] GetInvokeTarget(Type requiredType, string[] arguments) {
            if (arguments == null || arguments.Length == 0)
                return null;

            var name = arguments[0].ToLowerInvariant();
            var objects = GameObject.FindObjectsOfType(requiredType);
            return objects.Where(o => o.name.ToLowerInvariant().StartsWith(name)).ToArray();
        }

    }
}
