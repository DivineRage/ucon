﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UConsole.InstanceRetrieval {
    public class InstanceRetrievalAllObjects : InstanceRetrievalMethod {

        public override int RequiredArgumentCount {
            get {
                return 0;
            }
        }

        public override string[] RequiredArgumentNames {
            get {
                return new string[0];
            }
        }

        public override string[] RequiredArgumentUsageDescriptions {
            get {
                return new string[0];
            }
        }

        public override object[] GetInvokeTarget(Type requiredType, string[] arguments) {
            return GameObject.FindObjectsOfType(requiredType);
        }

    }
}